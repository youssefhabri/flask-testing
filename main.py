import os
import sys
import time
import json
from flask import Flask, request, jsonify
from OpenSSL import SSL

context = ('localhost.crt', 'localhost.key')

app = Flask(__name__)

def log_to_file(data):
    with open('requests.log', 'a+') as file:
        file.writelines(data + '\n')

@app.route('/', defaults={'path': ''}, methods=['GET', 'POST'])
@app.route('/<path:path>', methods=['GET', 'POST'])
def catch_all(path):
    data = request.form
    log_to_file(path+':'+json.dumps(data))
    return jsonify(data)
    # return path

app.run(
    host='127.0.0.1',
    port=80,
    debug=True,
    threaded=True)
